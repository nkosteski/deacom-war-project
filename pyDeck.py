from pyCard import Card
import random

class Deck:
    def __init__(self, numDecks=0, aceHigh = True, hasJokers = False):
        self.cards = []
        self.highestCard = Card(-1, '\t')
        self.lowestCard = Card(100, '\t')

        for i in range(numDecks): #creates a new list of cards with suits, display, and ranks
            for suit in ['spades ', 'hearts ', 'diamonds ', 'clubs ']:#why are there spaces
                for rank in range(1,14):
                    self.cards.append(Card(rank, suit, aceHigh))
            if hasJokers: #inelegant addition of Jokers
                self.cards.append(Card(15, '\t'))
                self.cards.append(Card(15, '\t'))

    @classmethod
    def _deckFromCards(cls, cards):
        newCls = cls()
        newCls.cards = cards
        return newCls

    def __str__(self):
        printStr = ''
        for card in self.cards:
            printStr += str(card) + '\n'
        return(printStr[0:-1])

    def __len__(self):
        return len(self.cards)

    def __add__(self, other):
        return self.cards + other.cards
        self.updateHighCard()
        self.updateLowCard()

    def __iadd__(self, other):
        self.cards += other.cards
        self.updateHighCard()
        self.updateLowCard()
        return self

    def __getitem__(self, key):
        if isinstance(key,int):
            return self.cards[key]
        return self._deckFromCards(self.cards[key])

    def addToBottom(self, card):
        self.cards.append(card)
        self.updateHighCard()
        self.updateLowCard()

    def addToTop(self, card):
        self.cards.insert(0, card)
        self.updateHighCard()
        self.updateLowCard()

    def drawFromTop(self):
        returnCard = self.cards.pop(0)
        self.updateHighCard()
        self.updateLowCard()
        return returnCard

    def drawFromBottom(self):
        returnCard = self.cards.pop()
        self.updateHighCard()
        self.updateLowCard()
        return returnCard

    def clear(self):
        self.cards = []

    def shuffle(self, bridgeShuffle = False, shuffles = 7):
        if bridgeShuffle:
            err = round(len(self)*.15)
            for i in range(shuffles):
                error = random.randint(-err,err) #lazy attempt at human error in splitting a deck in half
                halfDeck1 = self[:len(self)//2+error]
                halfDeck2 = self[len(self)//2+error:]
                self.clear()
                while(len(halfDeck1)>0 or len(halfDeck2)>0):
                    p = len(halfDeck1)/(len(halfDeck1)+len(halfDeck2)) #attempt to determine probability of a card falling from the left hand vs right hand deck
                    if (random.random() < p):
                        self.cards.insert(0, halfDeck1.cards.pop())
                    else:
                        self.cards.insert(0, halfDeck2.cards.pop())
        else:
            for i in range(shuffles):
                random.shuffle(self.cards) #built-in python list shuffle

    def dealDeck(self, numberOfHands = 2):
        #deals out hands in the same way a person would returning a list of decks
        retList = []
        counter = 0
        for i in range(numberOfHands):
            retList.append(Deck())
        while len(self) > 0:
            retList[counter%numberOfHands].addToBottom(self.cards.pop(0))
            counter+=1
        return retList

    def getHighestCardIndexes(self):
        #gets the indexes of all of the highest ranked cards in the deck
        highCard = max(self.cards)
        retIndex = []
        for i in range(len(self.cards)):
            if highCard == self.cards[i]:
                retIndex.append(i)
        return retIndex

    def updateHighCard(self):
        try:
            self.highestCard = max(self.cards)
        except ValueError:
            self.highestCard = None

    def updateLowCard(self):
        try:
            self.lowestCard = min(self.cards)
        except ValueError:
            self.lowestCard = None
