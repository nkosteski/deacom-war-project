from pyPlayer import Player
from pyDeck import Deck

class WarEngine:
    def __init__(self, gameOptions):
        self.players = []
        self.eleminatedPlayers = []
        self.autoplay = gameOptions['Autoplay']
        self.winningCardToBottom = gameOptions['Winning Cards go to'] == 'Bottom'
        self.bridgeShuffle = gameOptions['Type of Shuffle'] == 'Bridge'
        self.round = 1
        self.gamesOfWar = 0

        startingDeck = Deck(gameOptions['Number of Decks'], gameOptions['Ace High'], gameOptions['Use Jokers'])
        startingDeck.shuffle(self.bridgeShuffle)

        playerNumber=1
        for deck in startingDeck.dealDeck(gameOptions['Number of Players']):
            self.players.append(Player(playerNumber, deck, self.winningCardToBottom, self.bridgeShuffle))
            playerNumber+=1

    def compareHand(self, playersInvolved):
        #compares the players draws and returns the winner, if there is a war it stages a war
        cardPile = Deck()
        for player in playersInvolved:
            cardPile.addToBottom(player.playCard())

        try:
            winners = cardPile.getHighestCardIndexes()
            winner = None

            if not self.autoplay:
                self.displayHand(playersInvolved, cardPile)

            if len(winners) > 1:
                warPlayers = []
                for i in winners:
                    warPlayers.append(playersInvolved[i])
                self.eleminatePlayers()
                winner = self.warDeclared(warPlayers)
            else:
                winner = playersInvolved[winners[0]]

            cardPile.shuffle(bridgeShuffle = False, shuffles = 1)
            winner.wonCards(cardPile)
            self.eleminatePlayers()

            return winner

        except (ValueError, AttributeError):
            #catches the tie error conditions
            return None

    def warDeclared(self, warPlayers):
        #sets up the length of a war (up to four cards including the one drawn to determine winner)
        #creates the bonus pile for the hand, then determines the winner
        self.gamesOfWar += 1
        warLength = min(map(lambda x: x.cardCount(), warPlayers))
        warLength = 4 if warLength > 4 else warLength

        if warLength == 0:
            for player in warPlayers:
                if player in self.playersEliminated:
                    warPlayers.remove(player)
            if len(warPlayers) > 1:
                return self.warDeclared(warPlayers)

        bonusCards = Deck()
        for player in warPlayers:
            for i in range(warLength-1):
                bonusCards.addToBottom(player.playCard())

        if not self.autoplay:
            self.displayWarHand(warPlayers, bonusCards)

        bonusCards.shuffle(bridgeShuffle = False, shuffles = 1)

        try:
            winner = self.compareHand(warPlayers)
            winner.wonCards(bonusCards)
            winner.warsWon+=1
            return winner

        except AttributeError:
            #catches if all players are not able to compete in a war i.e. they ran out of cards
            return None


    def eleminatePlayers(self):
        #eliminates players with no cards left from the game
        playersEliminated = []
        for player in self.players:
            if player.cardCount() == 0:
                playersEliminated.append(player)

        for player in playersEliminated:
            print("Player " + str(player.id) + " has lost in round " + str(self.round) + ".")
            self.eleminatedPlayers.append((player, self.round))
            self.players.remove(player)

        if len(self.players) == 0:
            tieString='\nWait a second Players: \n\t'
            for player in playersEliminated:
                tieString+= str(player.id) + ', '
            tieString = tieString[:-2] + ('\nHave ended in a tie, Congrats!')
            print(tieString)

        return playersEliminated

    def playHand(self):
        #plays one round of the game
        try:
            winner = self.compareHand(self.players)
            winner.handsWon+=1

            if not self.autoplay:
                print('\nPlayer ' + str(winner.id) + ' has won round ' + str(self.round)+ '.\n\n')

            self.round+=1
        except AttributeError:
            return None


    #the following functions are for display purposes


    def displayHand(self, players, hand):
        statString = '\n\t'
        for player in players:
            statString+= 'Player ' + str(player.id) + '\t'

        statString+='\n\t'
        for card in hand:
            statString+=str(card)+'\t'

        print(statString)

    def displayWarHand(self, players, bonusCards):
        numPlayers = len(players)

        warString = "\t\tWAR DECLARED! Next hand's bonus:\n\t\t"
        for player in players:
            warString+= 'Player ' + str(player.id) + '\t\t'

        warString += '\n\t\t'

        i=0
        while i < len(bonusCards):
            for j in range(numPlayers):
                warString += '*' + str(bonusCards[i]) + '\t\t'
                i+=1
            warString += '\n\t\t'

        print(warString)

    def displayStatistics(self, printLosers = True):
        if printLosers and len(self.eleminatedPlayers) > 0:
            print('\nDefeated Player Statistics:')
            for player, round in self.eleminatedPlayers:
                print(player)
                print('\t\tLost in round ' + str(round))
                print('')

        print('\nActive Player Statistics:')
        for player in self.players:
            print(player)
            print('')
