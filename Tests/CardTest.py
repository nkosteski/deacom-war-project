import unittest
from pyCard import Card

class TestCard(unittest.TestCase):

    def test_create_card(self):
        for i in range(1,16):
            newCard = Card(i, '')
        self.assertTrue(True)

    def test_ace_low(self):
        newCard = Card(1, '', False)
        self.assertTrue(newCard.card == 'A' and newCard.rank == 1)

    def test_ace_high(self):
        newCard = Card(1, '', True)
        self.assertTrue(newCard.card == 'A' and newCard.rank == 14)

    def test_joker(self):
        newCard = Card(15, '')
        self.assertTrue(newCard.card == 'Joker' and newCard.rank == 15)

    def test_comparison_less(self):
        lowCard = Card(2,'')
        highCard = Card(10,'')
        self.assertTrue(lowCard < highCard)
        self.assertFalse(highCard < lowCard)

    def test_comparison_greater(self):
        lowCard = Card(3,'')
        highCard = Card(12,'')
        self.assertFalse(lowCard > highCard)
        self.assertTrue(highCard > lowCard)

    def test_equals(self):
        self.assertTrue(Card(3,'') == Card(3,''))
        self.assertFalse(Card(4,'') == Card(3,''))

    def test_display(self):
        self.assertTrue(str(Card(1, 'Heart')) == 'A Heart')

if __name__ == '__main__':
    unittest.main()
