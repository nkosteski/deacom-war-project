import unittest
from pyDeck import Deck
from pyCard import Card

class TestDeck(unittest.TestCase):

    def test_create_empty_deck(self):
        d = Deck()
        self.assertTrue(len(d) == 0)

    def test_create_full_deck(self):
        d = Deck(1)
        self.assertTrue(len(d) == 52)

    def test_create_joker_deck(self):
        d = Deck(1, hasJokers=True)
        self.assertTrue(len(d) == 54)
        self.assertTrue(d[-1].card == "Joker")

    def test_ace_high_deck(self):
        d=Deck(1, aceHigh = True)
        self.assertTrue(d[0].rank == 14)

    def test_ace_low_deck(self):
        d=Deck(1, aceHigh = False)
        self.assertTrue(d[0].rank == 1)

    def test_len(self):
        d= Deck(1)
        d.drawFromTop()
        d.drawFromTop()
        self.assertTrue(len(d) == 50)

    def test_add(self):
        d=Deck() + Deck(1)
        self.assertTrue(len(d) == 52)

    def test_inplace_add(self):
        d=Deck()
        d+=Deck(1)
        self.assertTrue(len(d) == 52)

    def test_get_item(self):
        d = Deck(1)
        self.assertTrue(str(d[1]) == '2 spades ')

    def test_get_slice(self):
        d = Deck(1)
        print(d[1:3])
        self.assertTrue(str(d[1:3]) == '2 spades \n3 spades \n')

    def test_add__to_top(self):
        d=Deck(1)
        card = Card(3, 'Balloons')
        d.addToTop(card)
        self.assertTrue(str(d[0])=='3 Balloons')
        self.assertTrue(len(d) == 53)

    def test_add__to_bottom(self):
        d=Deck(1)
        card = Card(3, 'Balloons')
        d.addToBottom(card)
        self.assertTrue(str(d[-1])=='3 Balloons')
        self.assertTrue(len(d) == 53)

    def test_draw_from_top(self):
        d=Deck(1)
        card = d.drawFromTop()
        self.assertTrue(str(card) == 'A spades ')
        self.assertTrue(len(d) == 51)

    def test_draw_from_bottom(self):
        d=Deck(1)
        card = d.drawFromBottom()
        self.assertTrue(str(card) == 'K clubs ')
        self.assertTrue(len(d) == 51)

    def test_clear(self):
        d=Deck(3)
        self.assertFalse(len(d)==0)
        d.clear()
        self.assertTrue(len(d)==0)

    def test_python_shuffle(self):
        sorted = Deck(1)
        shuffled = Deck(1)
        shuffled.shuffle()
        unsorted = False
        for control,test in zip(sorted, shuffled):
            if control != test:
                unsorted = True
                break
        self.assertTrue(unsorted)

    def test_bridge_shuffle(self):
        sorted = Deck(1)
        shuffled = Deck(1)
        shuffled.shuffle(bridgeShuffle = True)
        unsorted = False
        for control,test in zip(sorted, shuffled):
            if control != test:
                unsorted = True
                break
        self.assertTrue(unsorted)

    def test_deal_two_players(self):
        d = Deck(1)
        p1, p2 = d.dealDeck()
        self.assertTrue(len(p1) == 26)
        self.assertTrue(len(p2) == 26)

    def test_deal_uneven_hands(self):
        d = Deck(1)
        p1, p2, p3 = d.dealDeck(numberOfHands=3)
        self.assertTrue(len(p1) == 18)
        self.assertTrue(len(p2) == 17)
        self.assertTrue(len(p2) == 17)

    def test_get_high_card_single(self):
        d=Deck()
        d.addToBottom(Card(2,''))
        d.addToBottom(Card(4,''))
        highCardIndex = d.getHighestCardIndexes()
        self.assertTrue(highCardIndex == [1])

    def test_get_high_card_multi(self):
        d=Deck()
        d.addToBottom(Card(4,''))
        d.addToBottom(Card(2,''))
        d.addToBottom(Card(4,''))
        d.addToBottom(Card(2,''))
        d.addToBottom(Card(2,''))
        highCardIndex = d.getHighestCardIndexes()
        self.assertTrue(highCardIndex == [0,2])

    def test_update_high_card(self):
        d=Deck()
        d.addToBottom(Card(4,''))
        d.addToBottom(Card(2,''))
        prevHigh = d.highestCard
        d.addToBottom(Card(6,''))
        self.assertTrue(d.highestCard == Card(6,''))
        self.assertTrue(d.highestCard > prevHigh)

    def test_update_low_card(self):
        d=Deck()
        d.addToBottom(Card(6,''))
        d.addToBottom(Card(4,''))
        prevLow = d.lowestCard
        d.addToBottom(Card(2,''))
        self.assertTrue(d.lowestCard == Card(2,''))
        self.assertTrue(d.lowestCard < prevLow)

    def test_display(self):
        pass


if __name__ == '__main__':
    unittest.main()
