from pyDeck import Deck

class Player:
    def __init__(self, playerId, deck, winsToBottom=True, bridgeShuffle=False, numberShuffles = 7):
        self.id = playerId
        self.deck = deck
        self.winsToBottom = winsToBottom
        self.bridgeShuffle = bridgeShuffle
        self.numberShuffles = numberShuffles
        self.wonCardPool = Deck()
        self.highestCardCount = len(deck)
        self.lowestCardCount = len(deck)
        self.handsWon = 0
        self.warsWon = 0
        self.highestCardEver = max(deck)
        self.lowestCardEver = min(deck)

    def __str__(self):
        printStr = "\tPlayer " + str(self.id) + "'s statistics:\n" \
        "\t\tCurrent Card Count: " + str(self.cardCount()) + "\n" \
        "\t\tHighest Card Count: " + str(self.highestCardCount) + "\n" \
        "\t\tHighest Card Ever: " + str(self.highestCardEver) + "\n" \
        "\t\tHighest Card Owned: " + str(self.determineHighestCard()) + "\n" \
        "\t\tLowest Card Count: " + str(self.lowestCardCount) + "\n" \
        "\t\tLowest Card Ever: " + str(self.lowestCardEver) + "\n" \
        "\t\tLowest Card Owned: " + str(self.determineLowestCard()) + "\n" \
        "\t\tHands Won: " + str(self.handsWon) + "\n" \
        "\t\tWars Won: " + str(self.warsWon)
        return(printStr)

    def playCard(self):
        if self.cardCount() > 0:
            if self.cardCount() - 1 < self.lowestCardCount:
                self.lowestCardCount = self.cardCount()-1
            try:
                return self.deck.drawFromTop()
            except IndexError: #if there's a card pool the fail-over would be to draw from the previously won cards
                self.shuffle()
                self.deck += self.wonCardPool
                self.wonCardPool.clear()
                return self.deck.drawFromTop()
        else:
            return None

    def shuffle(self):
        self.wonCardPool.shuffle(self.bridgeShuffle, self.numberShuffles)

    def wonCards(self, cards):
        #takes the card pile and adds it either to the player's bottom of the deck or card pool
        if self.winsToBottom:
            self.deck += cards
        else:
            self.wonCardPool += cards

        if self.cardCount() > self.highestCardCount:
            self.highestCardCount = self.cardCount()

        self.updateHighCard()
        self.updateLowCard()

    def cardCount(self):
        return len(self.deck) + len(self.wonCardPool)

    def updateHighCard(self):
        if self.determineHighestCard() >= self.highestCardEver:
            self.highestCardEver = self.determineHighestCard()

    def updateLowCard(self):
        if self.determineLowestCard() <= self.lowestCardEver:
            self.lowestCardEver = self.determineLowestCard()

    def determineHighestCard(self):
        if self.winsToBottom:
            return self.deck.highestCard
        else:
            highCard = None
            if len(self.deck) > 0:
                highCard = self.deck.highestCard
            if len(self.wonCardPool) > 0:
                if highCard:
                    if self.wonCardPool.highestCard > highCard:
                        highCard = self.wonCardPool.highestCard
                else:
                    highCard = self.wonCardPool.highestCard
            return highCard


    def determineLowestCard(self):
        if self.winsToBottom:
            return self.deck.lowestCard
        else:
            lowCard = None
            if len(self.deck) > 0:
                lowCard = self.deck.lowestCard
            if len(self.wonCardPool) > 0:
                if lowCard:
                    if self.wonCardPool.lowestCard < lowCard:
                        lowCard = self.wonCardPool.lowestCard
                else:
                    lowCard = self.wonCardPool.lowestCard

            return lowCard
