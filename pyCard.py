class Card:
    def __init__(self, rank, suit, aceHigh=True):
        self.suit = suit
        self.rank = rank

        if rank == 1: #why didn't I do 1 or 14?
            self.card ='A'
            if aceHigh:
                self.rank = 14
        elif rank == 11:
            self.card = 'J'
        elif rank == 12:
            self.card = 'Q'
        elif rank == 13:
            self.card = 'K'
        elif rank == 15:
            self.card = 'Joker'
        else:
            self.card = str(self.rank)

    def __lt__(self,other):
        return self.rank < other.rank

    def __le__(self,other):
        return self.rank <= other.rank

    def __eq__(self,other):
        return self.rank == other.rank

    def __gt__(self,other):
        return self.rank >= other.rank

    def __ge__(self,other):
        return self.rank >= other.rank

    def __str__(self):
        return self.card + ' ' + self.suit
