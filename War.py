from pyWarEngine import WarEngine

gameOptions = {
'Number of Players':2,
'Number of Decks':1,
'Ace High': True,
'Use Jokers': False,
'Type of Shuffle': 'Standard',
'Winning Cards go to' : 'Bottom',
'Autoplay' : True
}

MIN_DECKS = 1
MAX_DECKS = 16

MIN_PLAYERS = 2
MAX_PLAYERS =  gameOptions['Number of Decks']* (26 + gameOptions['Use Jokers'])

print('\nWelcome to WAR!\n')

exitCode = ''
while (exitCode.lower() != 'exit'):
    changePrompt = ''
    while (changePrompt != 'Y' and changePrompt != 'N' and exitCode != 'exit'):
        changePrompt = input('\nBefore you start a new game, would you like change the game settings? (Y/N)\n>>> ')
        if len(changePrompt)>0:
            exitCode = changePrompt
            changePrompt = changePrompt[0].upper()
        if changePrompt == 'Y':
            setting = ''
            while setting != 'done':
                print('\nWhich setting would you like to change?\n(Please enter the number of the setting you want to change or type "done" to start a new game)\n')
                i=1
                for option in gameOptions:
                    print('\t' + str(i) + '.) ' + option + ': ' + str(gameOptions[option]))
                    i+=1
                setting = input('\n>>> ')
                if setting == '1':
                    players = 0
                    while (players < MIN_PLAYERS or players > MAX_PLAYERS):
                        players = input('\nHow many players are playing?\n(Enter a number ' + str(MIN_PLAYERS) + '-' + str(MAX_PLAYERS) +')\n>>> ')
                        try:
                            players = int(players)
                            gameOptions[list(gameOptions.keys())[int(setting)-1]] = players
                        except ValueError:
                            print('Not a valid player number please try again!\n')
                            players = 0

                if setting == '2':
                    decks = 0
                    while (decks < MIN_DECKS or decks > MAX_DECKS):
                        decks = input('\nHow many decks are being used?\n(Enter a number ' + str(MIN_DECKS) + '-' + str(MAX_DECKS) + ')\n>>> ')
                        try:
                            decks = int(decks)
                            gameOptions[list(gameOptions.keys())[int(setting)-1]] = decks
                        except ValueError:
                            print('Not a valid input please try again!\n')
                            decks = 0

                if setting == '3':
                    aceHigh = ''
                    while (aceHigh == ''):
                        aceHigh = input('\nWould you like aces to be the high card?\n(Y/N)\n>>> ')
                        if len(aceHigh)>0:
                            aceHigh = aceHigh[0].upper()
                            if aceHigh == 'Y' or aceHigh == 'N':
                                gameOptions[list(gameOptions.keys())[int(setting)-1]] = aceHigh == 'Y'
                            else:
                                print('Invalid input please try again!\n')
                                aceHigh = ''

                if setting == '4':
                    jokers = ''
                    while (jokers == ''):
                        jokers = input('\nWould you like to use jokers?\n(Y/N)\n>>> ')
                        if len(jokers)>0:
                            jokers = jokers[0].upper()
                            if jokers == 'Y' or jokers == 'N':
                                gameOptions[list(gameOptions.keys())[int(setting)-1]] = jokers == 'Y'
                            else:
                                print('Invalid input please try again!\n')
                                jokers = ''

                if setting == '5':
                    shuffleType = ''
                    while (shuffleType == ''):
                        shuffleType = input('\nWhat type of shuffle would you like to do?\nEnter B for Bridge (real life card shuffle)\nEnter S for a standard randomly generated shuffle\n>>> ')
                        if len(shuffleType)>0:
                            shuffleType = shuffleType[0].upper()
                            if shuffleType == 'B':
                                gameOptions[list(gameOptions.keys())[int(setting)-1]] = 'Bridge'
                            elif shuffleType == 'S':
                                gameOptions[list(gameOptions.keys())[int(setting)-1]] = 'Standard'
                            else:
                                print('Invalid input please try again!\n')
                                shuffleType = ''

                if setting == '6':
                    replaceType = ''
                    while (replaceType == ''):
                        replaceType = input('\nWhen cards are won where should they go?\nEnter B for won cards to be placed on the bottom of the deck\nEnter P for won cards to be placed in a seperate deck then reshuffled before being played again\n>>> ')
                        if len(replaceType)>0:
                            replaceType = replaceType[0].upper()
                            if replaceType == 'B':
                                gameOptions[list(gameOptions.keys())[int(setting)-1]] = 'Bottom'
                            elif replaceType == 'P':
                                gameOptions[list(gameOptions.keys())[int(setting)-1]] = 'Pool'
                            else:
                                print('Invalid input please try again!\n')
                                replaceType = ''

                if setting == '7':
                    autoplay = ''
                    while (autoplay == ''):
                        autoplay = input('\nWould you like the game to do an automatic playthrough?\n(Y/N)\n>>> ')
                        if len(autoplay)>0:
                            autoplay = autoplay[0].upper()
                            if autoplay == 'Y' or autoplay == 'N':
                                gameOptions[list(gameOptions.keys())[int(setting)-1]] = autoplay == 'Y'
                            else:
                                print('Invalid input please try again!\n')
                                autoplay = ''

                MAX_PLAYERS = gameOptions['Number of Decks'] * (26 + gameOptions['Use Jokers'])
                if gameOptions['Number of Players'] > MAX_PLAYERS:
                    print('\n***WARNING: Number of players set to new max of '+str(MAX_PLAYERS)+'***\n')
                    gameOptions['Number of Players'] = MAX_PLAYERS


    if exitCode.lower() != 'exit':
        print('\nNew game starting...')
        newGame = WarEngine(gameOptions)
        exitCode = input("..new game loaded.\n\nIf at any time you want to exit you game type 'exit' into the prompt.\nPress enter to continue.\n>>> ")

        while (exitCode != 'exit' and len(newGame.players) > 1):
            if not gameOptions['Autoplay']:
                print('Round ' + str(newGame.round))
                exitCode = input("Press enter to continue. To display active player statistics enter 'stats'\n>>> ")

            if exitCode.lower() == 'stats':
                newGame.displayStatistics(printLosers=False)
            else:
                newGame.playHand()

        newGame.displayStatistics()
        if len(newGame.players) == 1:
            print('\nPlayer '+ str(newGame.players[0].id) + ' has won the game in '+str(newGame.round)+' rounds!!!\n')

print('Thank you for playing. Goodbye! :)')
